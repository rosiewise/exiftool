-tagsFromFile @
# Copy artist name where it should be...
-Copyright<© $Artist, all rights reserved
-CopyrightNotice<Copyright
-ArtworkCopyrightNotice=Please respect the work of others and contact me if you wish to use this image
-ArtworkCreator<Artist
-CopyrightOwnerName<Artist
-d %Y -CreditLine<© $DateTimeOriginal $Artist ($CreatorWorkEmail), all rights reserved
#-CreditLine<© $DateTimeOriginal $Artist ($CreatorWorkEmail), all rights reserved