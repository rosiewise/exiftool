The cmd files in this (and child directories, with the exception of templates) are used in shortcuts to apply a specific
set of predefined rules to change the meta data of files (or files in directories) dropped onto the link.

Ultimately these cmd files are what make the "drop zones" work as they do.

If you want to have a go at writing these for yourself, please refer to the ExifTool documentation for help.