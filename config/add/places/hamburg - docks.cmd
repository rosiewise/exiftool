-LocationShownCountryCode=DEU
-LocationShownCountryName=Germany
-LocationShownCity=Hamburg
-LocationShownProvinceState=Hamburg
-LocationShownSublocation=St. Pauli
-Event=Stereophonics @ Docks
-GPSAltitude=20 m
-GPSAltitudeRef=Above Sea Level
-GPSLatitude=53 deg 32' 57.35"
-GPSLatitudeRef=North
-GPSLongitude=9 deg 57' 52.27"
-GPSLongitudeRef=East

# c:\exiftool\exiftool.exe -k -r -@ "config\add\people\hamburg - docks.cmd"