-LocationShownCountryCode=DEU
-LocationShownCountryName=Germany
-LocationShownCity=Frankfurt am Main
-LocationShownProvinceState=Hessen
-LocationShownSublocation=Frankfurt am Main
-Event=Stereophonics @ Gibson
-GPSAltitude=114 m
-GPSAltitudeRef=Above Sea Level
-GPSLatitude=50 deg 6' 50.81"
-GPSLatitudeRef=North
-GPSLongitude=8 deg 40' 56.43"
-GPSLongitudeRef=East