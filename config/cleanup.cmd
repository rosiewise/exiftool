-MakerNotes:all=
-IFD1:all=
-InteropIFD:all=
-PrintIM:all=
-mpf:all=
-trailer:all=

# Remove pet name/age from images
-PanasonicTitle=
-PanasonicTitle2=

# Remove pointless image bits and pieces
-ExposureProgram=
-MaxApertureValue=
-MeteringMode=
-LightSource=
-Flash=
-FocalLength=
-SensingMethod=
-ExposureMode=
-WhiteBalance=
-DigitalZoomRatio=
-FocalLengthIn35mmFormat=
-Contrast=
-Saturation=
-Sharpness=
-GainControl=
-SceneCaptureType=
-ExifVersion=
-ExifImageWidth=
-ExifImageHeight=
-ExifOffset=
-ColorSpace=
-CustomRendered=
-FlashPixVersion=
-FNumber=
-ExposureTime=
-ExposureBiasValue=
-ISOSpeedRatings=
#-Orientation=
-XResolution=
-YResolution=
-ResolutionUnit=

-IFD0:Padding=
-ExifIFD:Padding=
-ExifIFD:OffsetSchema=

-FileSource=Digital Camera
-SceneType=Directly photographed

-XMP-exif:all=
-XMP-rdf:About=
-XMP-xmp:all=

# Start putting in some standard values
-Software=ExifTool, Paint.net and IrfanView
-Artist=Rosemarie Wise
-CreatorWorkEmail=rosie@websiteowner.info
-CreatorWorkURL=rosiesgigphotos.co.uk
-CreatorRegion=Lancashire
-CreatorCountry=UK
-ImageFileConstraints=Maintain Metadata
-ImageFileFormatAsDelivered=JPEG Interchange Formats (JPG, JIF, JFIF)
-TermsAndConditionsText=If you use this image in anyway, please be courteous and let me know (rosie@rosiesgigphotos.co.uk), I like to see how my images are being used.
-ArtworkCopyrightNotice=Please respect the work of others and contact me if you wish to use this image