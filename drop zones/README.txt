It is strongly recommended that you make a backup of any images you process - however, the extract thumbnails step will
create a copy of the file in it's source directory with "_original" tagged onto the extension as well as creating a new
folder in the source directory called web, where any extracted thumbnails will go.

Drag an image, image set, or folder containing images onto the drop zone to process those instructions on
qualifying files. You should work on them in numerical order, optionally adding any people/places (kept seperate in the
add folder) after step 2 (Populate More) but before step 3 (Build Keywords).

To check the tags of images at various stages to make sure changes are being applied, you can use the appropriate drop
zones in the debug folders depending on quantity/choice of output; however be aware that there is no cleanup drop zones
set up and you will need to delete .html/.txt files manually or reconfigure the cmd in the shortcut you wish to
regenerate/overwrite them.

TODO Command line as it is in each of the links.