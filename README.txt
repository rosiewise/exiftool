This repository is online primarily to demonstrate how I make use of ExifTool to process some of my images before
putting them online. They are provided on an AS IS basis with the intention that someone might want to recreate my own
setup or to use these examples to facilitate their own learning of how they may wish to use ExifTool.

RUNNING THE EXAMPLES
--------------------
1.  Download ExifTool from http://www.sno.phy.queensu.ca/~phil/exiftool/ as a Windows standalone executable.
2.  Extract exiftool(-k).exe to C:\exif
3.  Rename exiftool(-k).exe to exiftool.exe
4.  Clone/download this repository to C:\exif
5.  If you wish to use a different location, you may need to update the shortcuts (.lnk files) individually to get them
    to work.

ORGANISATION
------------
Hopefully the names are descriptive enough to work out what various things do. There are currently 3 directories,
processing happens in the drop zones folders, it is recommended that you configure anything in this folder and any sub
folders to use the large icon view to make suitably sized drop zones for use.

config - contains the config files, these files control behaviour for the more complicated drop zones.
drop zones - sets of shortcuts that you can drop supported files, or folders containing supported files for processing.
tag information - shortcuts listing the names of tags in various groups, just click for the group list